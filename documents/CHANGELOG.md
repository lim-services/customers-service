# Change logs
*1.0.0*
- Project Creation
- Create the Customer Service
- Write Unit Tests
- Add Swagger
- Add e2e test using Postman
- Add a DockerFile
- Add Postman Contract Test
- Add gitlab CI/CD