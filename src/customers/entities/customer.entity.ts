export class Customer {
  id: number;
  firstName: string;
  lastName: string;
  title: string;
  location: string[];
}
